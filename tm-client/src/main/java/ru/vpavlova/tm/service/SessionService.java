package ru.vpavlova.tm.service;

import lombok.*;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.endpoint.Session;

@Getter
@Setter
@Service
public class SessionService {

    @Nullable
    private Session session = null;

}
